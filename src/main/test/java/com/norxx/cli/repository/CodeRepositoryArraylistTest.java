package com.norxx.cli.repository;


import com.norxx.cli.model.Code;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class CodeRepositoryArraylistTest {


    @Test
    public void testSave() {
        CodeRepository codeRepository = new CodeRepositoryArraylist();
        Code expected = new Code("test", 1f, true);
        codeRepository.save(expected);
        List<Code> list = ((CodeRepositoryArraylist) codeRepository).getList();
        Code actual = list.get(0);
        assertEquals(expected, actual);
    }

}