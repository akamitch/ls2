package com.norxx.cli.factorys;

import com.norxx.cli.repository.CodeRepository;
import com.norxx.cli.service.CodeService;

public interface CodeServiceCreator {
    CodeService factoryMethod(CodeRepository repository);
}
