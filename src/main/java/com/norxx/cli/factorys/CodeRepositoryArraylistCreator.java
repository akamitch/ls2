package com.norxx.cli.factorys;

import com.norxx.cli.repository.CodeRepository;
import com.norxx.cli.repository.CodeRepositoryArraylist;

public class CodeRepositoryArraylistCreator implements CodeRepositoryCreator {
    public CodeRepository factoryMethod(){
        return new CodeRepositoryArraylist();
    }
}
