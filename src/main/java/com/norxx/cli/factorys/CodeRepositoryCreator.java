package com.norxx.cli.factorys;

import com.norxx.cli.repository.CodeRepository;

public interface CodeRepositoryCreator {
    CodeRepository factoryMethod();
}
