package com.norxx.cli.factorys;

import com.norxx.cli.repository.CodeRepository;
import com.norxx.cli.service.CodeService;
import com.norxx.cli.service.CodeServiceImpl;

public class CodeServiceImplCreator implements CodeServiceCreator {
    public CodeService factoryMethod(CodeRepository repository){
        return new CodeServiceImpl(repository);
    }
}
