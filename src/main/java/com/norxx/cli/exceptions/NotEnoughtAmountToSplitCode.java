package com.norxx.cli.exceptions;

public class NotEnoughtAmountToSplitCode extends CodeExceptions {
    public NotEnoughtAmountToSplitCode() {
    }

    public NotEnoughtAmountToSplitCode(String msg) {
        super(msg);
    }

}
