package com.norxx.cli.exceptions;

class CodeExceptions extends RuntimeException {
    CodeExceptions() {
    }

    CodeExceptions(String msg) {
        super(msg);
    }
}
