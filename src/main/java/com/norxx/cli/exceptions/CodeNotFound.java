package com.norxx.cli.exceptions;

public class CodeNotFound extends CodeExceptions {
    public CodeNotFound() {
    }

    public CodeNotFound(String msg) {
        super(msg);
    }
}
