package com.norxx.cli.exceptions;

public class AlreadyUsedCode extends CodeExceptions {
    public AlreadyUsedCode() {
    }

    public AlreadyUsedCode(String msg) {
        super(msg);
    }
}
