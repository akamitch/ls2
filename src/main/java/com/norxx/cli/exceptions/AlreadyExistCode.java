package com.norxx.cli.exceptions;

public class AlreadyExistCode extends CodeExceptions {
    public AlreadyExistCode() {
    }

    public AlreadyExistCode(String msg) {
        super(msg);
    }
}
