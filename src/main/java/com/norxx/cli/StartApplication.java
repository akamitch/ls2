package com.norxx.cli;


import com.norxx.cli.factorys.CodeRepositoryArraylistCreator;
import com.norxx.cli.factorys.CodeRepositoryCreator;
import com.norxx.cli.factorys.CodeServiceCreator;
import com.norxx.cli.factorys.CodeServiceImplCreator;
import com.norxx.cli.model.Code;
import com.norxx.cli.model.DtoCode;
import com.norxx.cli.repository.CodeRepository;
import com.norxx.cli.repository.CodeRepositoryArraylist;
import com.norxx.cli.repository.CodeRepositoryPostgres;
import com.norxx.cli.service.CodeService;
import com.norxx.cli.service.CodeServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class StartApplication {
/*
Что еще надо сделать
- Репозиторий postgres
- заменить float на bigdecimal
- сделать rest api
 */
    public static void main(String[] args) {
        //CodeService cs = new CodeServiceImpl(new CodeRepositoryArraylist());
        CodeRepositoryCreator repositoryCreator = new CodeRepositoryArraylistCreator();
        CodeServiceCreator codeServiceCreator = new CodeServiceImplCreator();
        CodeRepository repository = repositoryCreator.factoryMethod();
        CodeService cs = codeServiceCreator.factoryMethod(repository);

        System.out.println("Создать новый код на сумму 19.55f");
        DtoCode dtoCode0 = cs.emitCode(19.55f);
        printDto(dtoCode0);

        System.out.println("------\nПерегенерить код");

        String codeString = dtoCode0.codeString;
        DtoCode dtoCode1 = cs.reGenerate(codeString);
        printDto(dtoCode1);

        System.out.println("------\nСделать из кода 4 новых на 1.1, 2.2, 3.3 и остаток");
        List<Float> listOfAmounts = new ArrayList<>();
        listOfAmounts.add(1.1f);
        listOfAmounts.add(2.2f);
        listOfAmounts.add(3.3f);

        List<DtoCode> dtoCodes = cs.split(dtoCode1.codeString, listOfAmounts);

        List<String> codeList = new ArrayList<>();
        for (DtoCode c : dtoCodes) {
            printDto(c);
            codeList.add(c.codeString);
        }

        System.out.println("------\nОбьеденить первые 3 кода списка в один новый");
        codeList.remove(3);
        printDto(cs.combine(codeList));
        System.out.println("end");
        /*
        */
    }

    private static void printDto(DtoCode dto) {
        System.out.println(dto.valid + " Code, Amount : " + dto.amount + " : " + dto.codeString);
    }
}
