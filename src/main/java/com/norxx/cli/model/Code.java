package com.norxx.cli.model;

import java.util.Objects;

public class Code {
    private Long id;
    private String codeString;
    private float amount;
    private boolean valid;

    public Code() {
    }

    public Code(String codeString, float amount, boolean valid){
        this.amount = amount;
        this.codeString = codeString;
        this.valid = valid;
    }

    public float getAmount(){
        return amount;
    }

    public String getCodeString() {
        return codeString;
    }


    public boolean isValid() {
        return valid;
    }

//    public String getCodeString() {
//        return codeString;
//    }

    public void setCodeString(String codeString) {
        this.codeString = codeString;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Code)) return false;
        Code code = (Code) o;
        return Float.compare(code.amount, amount) == 0 &&
                valid == code.valid &&
                Objects.equals(codeString, code.codeString);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codeString, amount, valid);
    }

    @java.lang.Override
    public java.lang.String toString() {
        String status;
        if (valid) {
            status = "isValid";
        } else {
            status = "Used";
        }
        return status + " Code, Amount : " + amount + " : " + codeString;
    }
}
