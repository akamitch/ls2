package com.norxx.cli.model;

public class DtoCode {
    public String codeString;
    public float amount;
    public boolean valid;

    public DtoCode(String codeString, float amount, boolean valid) {
        this.codeString = codeString;
        this.amount = amount;
        this.valid = valid;
    }
}
