package com.norxx.cli.repository;
import com.norxx.cli.model.Code;

public interface CodeRepository {
    Code save(Code code);
    void remove(Code code);
    Code findByCodeString(String codeString);
}
