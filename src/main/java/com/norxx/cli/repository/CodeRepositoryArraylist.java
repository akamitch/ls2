package com.norxx.cli.repository;

import com.norxx.cli.exceptions.AlreadyExistCode;
import com.norxx.cli.model.Code;

import java.util.ArrayList;
import java.util.List;

public class CodeRepositoryArraylist implements CodeRepository {
    private List<Code> list;

    public CodeRepositoryArraylist() {
        list = new ArrayList<>();
    }


    public List<Code> getList() {
        return list;
    }

    public Code save(Code code) throws AlreadyExistCode {

        String codeString = code.getCodeString();
        Code byCodeString = findByCodeString(codeString);
        if (byCodeString != null) {
            throw new AlreadyExistCode("Code already exists");
        }
        list.add(code);
        return code;
    }

    public void remove(Code code){
        list.remove(code);
    }

//    public void markUsed(Code code) throws CodeNotFound {
//        //TODO что то тут много кода получилось для такого простого действия
//        //вероятно я чего то упускаю в работе с ArrayList
//        boolean found = false;
//        int index = 0;
//        for (int i = 0; i < list.size(); i++) {
//            Code currentCode = list.get(i);
//            if (currentCode.getCode().equals(code.getCode())) {
//                found = true;
//                index = i;
//                break;
//            }
//        }
//        if (found) {
//            Code originalCode = list.get(index);
//            Code used = new Code(originalCode.getCode(), originalCode.getAmount(), false);
//            list.remove(index);
//            list.add(used);
//        } else {
//            throw new CodeNotFound();
//        }
//
//    }

    public Code findByCodeString(String codeString) {
        for (Code code : list) {
            if (code.getCodeString().equals(codeString)) {
                return code;
            }
        }
        return null;
    }
}
