package com.norxx.cli.service;

import com.norxx.cli.exceptions.AlreadyUsedCode;
import com.norxx.cli.exceptions.CodeNotFound;
import com.norxx.cli.exceptions.NotEnoughtAmountToSplitCode;
import com.norxx.cli.model.Code;
import com.norxx.cli.model.DtoCode;
import com.norxx.cli.repository.CodeRepository;

import java.util.ArrayList;
import java.util.List;

import static com.norxx.cli.service.SecureGeneratorService.generateSecureStringCode;

public class CodeServiceImpl implements CodeService {
    private final CodeRepository codeRepository;

    public CodeServiceImpl(CodeRepository repository) {
        this.codeRepository = repository;
        //TODO репозиторий вроде должен быть синглтоном, надо вспомнить как это достигается
    }

    private DtoCode toDto(Code code){
        return new DtoCode(code.getCodeString(), code.getAmount(), code.isValid());
    }

    public DtoCode emitCode(float amount){
        Code code = new Code(generateSecureStringCode(), amount, true);
        codeRepository.save(code);

        //TODO обернуть в try-catch для обработки коллизий если такой код уже есть в репозитории
        return toDto(code);
    }

    public DtoCode checkCode(String codeString) throws CodeNotFound {
        Code code = codeRepository.findByCodeString(codeString);
        return toDto(code);
    }

    public DtoCode reGenerate(String codeString) throws AlreadyUsedCode, CodeNotFound {

        Code code = codeRepository.findByCodeString(codeString);
        if (!code.isValid()) {
            throw new AlreadyUsedCode();
        }
        Code reGeneratedCode = new Code(generateSecureStringCode(), code.getAmount(), true);
        codeRepository.remove(code);
        code.setValid(false);
        codeRepository.save(code);

        codeRepository.save(reGeneratedCode);

        return toDto(reGeneratedCode);
    }

    public DtoCode combine(List<String> codeList) throws AlreadyUsedCode, CodeNotFound{
        float totalAmount = 0;
        for (String codeString : codeList) {
            Code currentCode = codeRepository.findByCodeString(codeString);
            if (!currentCode.isValid()) {
                throw new AlreadyUsedCode();
            }
            totalAmount += currentCode.getAmount();
        }
        Code result = new Code(generateSecureStringCode(), totalAmount, true);

        /*
        Если какой то код был не рабочим, мы хотим ничего не делать и сообщить о этом клиенту

        Если мы тут, значит не было exception AlreadyUsedCode
        еще раз обходим весь список и помечаем коды как использованные.
        При первом обходе этого делать нельзя тк какой то код может оказаться не
        рабочим.
         */
        for (String codeString : codeList) {
            Code currentCode = codeRepository.findByCodeString(codeString);
            if (!currentCode.isValid()) {
                throw new AlreadyUsedCode();
            }
            codeRepository.remove(currentCode);
            currentCode.setValid(false);
            codeRepository.save(currentCode);

        }
        codeRepository.save(result);
        return toDto(result);
    }

    public List<DtoCode> split(String codeString, List<Float> amounts)
            throws AlreadyUsedCode, CodeNotFound, NotEnoughtAmountToSplitCode{
        Code code = codeRepository.findByCodeString(codeString);
        if (!code.isValid()) {
            throw new AlreadyUsedCode();
        }

        float totalAmount = 0;
        for (float a : amounts) {
            totalAmount +=a;
        }

        if (totalAmount > code.getAmount()) {
            throw new NotEnoughtAmountToSplitCode();
        }
        codeRepository.remove(code);
        code.setValid(false);
        codeRepository.save(code);

        ArrayList<DtoCode> resultList = new ArrayList<>();

        for (float amount : amounts) {
            Code currentCode = new Code(generateSecureStringCode(), amount, true);
            codeRepository.save(currentCode);
            resultList.add(toDto(currentCode));
        }
        if (totalAmount < code.getAmount()) {
            //если сумма запрошенных новых кодов меньше чем разбиваемый код,
            //то на весь остаток суммы делаем еще один код
            Code restCode = new Code(generateSecureStringCode(), code.getAmount() - totalAmount, true);
            codeRepository.save(restCode);
            resultList.add(toDto(restCode));
        }
        return resultList;
    }
}
