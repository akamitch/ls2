package com.norxx.cli.service;

import java.security.SecureRandom;

public class SecureGeneratorService {
    private static int codeLenght = 25;
    public static String generateSecureStringCode(){
        String allowedSymbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        StringBuilder result = new StringBuilder(allowedSymbols.length());
        SecureRandom secureRandom = new SecureRandom();
        for (int i=0; i < codeLenght; i++) {
            //int randomSymbolNum = Math.abs(secureRandom.nextInt()) % allowedSymbols.length();
            int randomSymbolNum = secureRandom.nextInt(allowedSymbols.length());
            char randomChar = allowedSymbols.charAt(randomSymbolNum);
            result.append(randomChar);
        }
        return result.toString();
    }
}
