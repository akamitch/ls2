package com.norxx.cli.service;

import com.norxx.cli.exceptions.AlreadyUsedCode;
import com.norxx.cli.exceptions.CodeNotFound;
import com.norxx.cli.exceptions.NotEnoughtAmountToSplitCode;
import com.norxx.cli.model.DtoCode;

import java.util.List;

public interface CodeService {
    DtoCode emitCode(float amount);
    //эта функция осуществляет эмиссию новых денег и
    // должна быть доступна к использованию только админу.
    //как то ее надо защитить чтоб ни api ни фронтенд не могли ее просто так вызывать

    DtoCode checkCode(String codeString) throws CodeNotFound;

    DtoCode reGenerate(String codeString) throws AlreadyUsedCode, CodeNotFound;

    DtoCode combine(List<String> codeList) throws AlreadyUsedCode, CodeNotFound;

    List<DtoCode> split(String codeString, List<Float> amounts)
            throws AlreadyUsedCode, CodeNotFound, NotEnoughtAmountToSplitCode;
}
